import socket
import threading


sem_connections = threading.Semaphore()

def broadcast(message: str, sender_name: str) -> None:
    with sem_connections:
        for peer in connections.values():
            if peer['socket'] == local_data.conn:
                prefix = '[You]'
            else:
                prefix = sender_name
            try:
                peer['socket'].sendall(f'{prefix} {message}'.encode())
            except:
                continue
    

def manage_client(conn: socket, addr: tuple) -> None:
    local_data.conn = conn
    global has_to_stop
    with conn:
        conn.sendall(b'Welcome to ChattiPython!\n')
        conn.sendall(b'\nYou can change your name with "\\Name <name>"\n')
        conn.sendall(b'You can close this connection with "\\Quit"\n')
        conn.sendall(b'You can list connections with "\\List"\n')
        conn.sendall(b'\nEnjoy your visit! :)\n\n')
        while not has_to_stop:
            try:
                data = conn.recv(1024)
            except TimeoutError:
                continue
            if not data:
                break
            message = data.decode()
            if message.lower() == '\\quit':
                break
            elif message.lower() == '\\stop':
                has_to_stop = True
                break
            elif message.lower().startswith('\\name '):
                connections[addr]['name'] = f'[{message[6:]} <{addr[0]}:{addr[1]}>]'
                continue
            elif message.lower() == '\\list':
                with sem_connections:
                    for peer in connections.values():
                        conn.sendall(peer['name'].encode() + b'\n')
                    sem_connections.release()
            broadcast(message, connections[addr]['name'])
    with sem_connections:
        connections.pop(addr)

HOST = '127.0.0.1'
PORT = 65432
has_to_stop = False
connections = {}
local_data = threading.local()
socket.setdefaulttimeout(1)
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    # Define the port as reusable (so it can be bound again)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((HOST, PORT))
    s.listen()
    while not has_to_stop:
        try:
            conn, addr = s.accept()
        except TimeoutError:
            continue
        with sem_connections:
            connections[addr] = {'socket': conn, 'addr': addr, 'name': f'[Tuntematon <{addr[0]}:{addr[1]}>]'}
        threading.Thread(target=manage_client, args=(conn, addr)).start()