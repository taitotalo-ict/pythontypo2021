function part1(data) {
    let floor = 0;
    for (let x of data) {
        if (x == '(') floor++
        else floor--;
    }
    part1Elem.textContent = floor;
}

function part2(data) {
    let floor = position = 0;
    while (floor != -1) {
        floor += data[position++] == '(' ? 1 : -1;
    }
    part2Elem.textContent = position;
}

function calculate() {
   let data = inputElem.value;
   part1(data);
   part2(data);
}

function initialize() {
    part1Elem = document.getElementById('part1');
    part2Elem = document.getElementById('part2');
    inputElem = document.getElementById('input');
    let calcElem = document.getElementsByTagName('button')[0]
    calcElem.addEventListener('click', calculate);
}

let part1Elem, part2Elem, inputElem;
window.addEventListener('load', initialize);

