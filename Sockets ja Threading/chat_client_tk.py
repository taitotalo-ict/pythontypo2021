import socket
import threading
import sys
import tkinter


def receive_messages(s: socket) -> None:
    """
    Receive and print messages from server
    """
    while not has_to_stop:
        try:
            data = s.recv(1024)
        except TimeoutError:
            continue
        except:
            break
        if not data:
            # Palvelin sulki yhteyttä
            break
        msg_list.insert(tkinter.END, data.decode())


def send(event=None):
    global has_to_stop
    message = my_msg.get()
    my_msg.set('')
    if message.lower() == 'exit':
        # Käyttäjä sulkee yhteyttä
        has_to_stop = True
        top.quit()
    try:
        s.sendall(message.encode())
    except:
        has_to_stop = True
        top.quit()
    if message.lower() == '\\quit':
        has_to_stop = True
        top.quit()


def on_closing(event=None):
    s.sendall(b'\\quit')
    top.quit()

top = tkinter.Tk()
top.title("Chatter")

messages_frame = tkinter.Frame(top)
my_msg = tkinter.StringVar()  # For the messages to be sent.
my_msg.set("Type your messages here.")
scrollbar = tkinter.Scrollbar(messages_frame)  # To navigate through past messages.
# Following will contain the messages.
msg_list = tkinter.Listbox(messages_frame, height=15, width=50, yscrollcommand=scrollbar.set)
scrollbar.pack(side=tkinter.RIGHT, fill=tkinter.Y)
msg_list.pack(side=tkinter.LEFT, fill=tkinter.BOTH)
msg_list.pack()
messages_frame.pack()

entry_field = tkinter.Entry(top, textvariable=my_msg)
entry_field.bind("<Return>", send)
entry_field.pack()
send_button = tkinter.Button(top, text="Send", command=send)
send_button.pack()

top.protocol("WM_DELETE_WINDOW", on_closing)


HOST = '95.217.212.162'
PORT = 65432

has_to_stop = False
receiver_ended = False
sender_ended = False
socket.setdefaulttimeout(1)
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    try:
        s.connect((HOST, PORT))
    except:
        print('Server not available.')
        sys.exit(1)    

    s.sendall(b'\\Name Christian')
    threading.Thread(target=receive_messages, args=(s,)).start()
    #send_messages(s)
    tkinter.mainloop()

    # t = threading.Thread(target=send_messages, args=(s,))
    # t.start()
    # receive_messages(s)
    
