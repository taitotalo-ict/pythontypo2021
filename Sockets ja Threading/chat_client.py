import socket
import threading
import sys
import argparse


def receive_messages(s: socket.socket) -> None:
    """
    Receive and print messages from server
    """
    global has_to_stop
    while not has_to_stop:
        try:
            data = s.recv(1024)
        except TimeoutError:
            continue
        except:
            break
        if not data:
            # Palvelin sulki yhteyttä
            break
        print(data.decode())
    has_to_stop = True


def send_messages(s: socket.socket) -> None:
    """
    Ask user for input and send the input to the server
    """
    global has_to_stop
    while not has_to_stop:
        message = input('Message for the chat: ')
        if message.lower() == 'exit':
            # Käyttäjä sulkee yhteyttä
            break
        try:
            s.sendall(message.encode())
        except:
            break
        if message.lower() == '\\quit' or message.lower() == '\\stop':
            break
    has_to_stop = True


parser = argparse.ArgumentParser(description='Chat client')

#parser.add_argument('ip', help='IP address of the server')
parser.add_argument('--local', action='store_true', help="Use local chat server (Default: Internet server)")
args = parser.parse_args()

HOST = '95.217.212.162'
if args.local:
    HOST = '127.0.0.1'
PORT = 65432

has_to_stop = False
socket.setdefaulttimeout(0.5)
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    try:
        s.connect((HOST, PORT))
    except:
        print('Server not available.')
        sys.exit(1)    

    # t = threading.Thread(target=receive_messages, args=(s))
    # t.start()
    # send_messages(s)

    t = threading.Thread(target=send_messages, args=(s))
    t.start()
    receive_messages(s)

    # Wait for thread to end before closing socket, to avoid errors/exceptions
    t.join()
