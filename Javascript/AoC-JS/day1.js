const fs = require('fs');

const data = fs.readFileSync('input_2015_1.txt', encoding = 'utf-8');

let floor = 0;

// for (let i=0; i < data.length; i++) {
//     if (data[i] == '(') floor++
//     else floor--;
// }

for (let x of data) {
    if (x == '(') floor++
    else floor--;
}

// for (let i in data) {
//     if (data[i] == '(') floor++
//     else floor--;
// }

// floor = data.split('(').length - data.split(')').length

// floor = data.match(/\(/g).length - data.match(/\)/g).length

console.log(`Part 1: ${floor}`);

floor = 0;
position = 0;

// do {
//     // if (data[position] == '(') floor++
//     // else floor--;
//     floor += data[position++] == '(' ? 1 : -1;
// } while (floor != -1);

while (floor != -1) {
    floor += data[position++] == '(' ? 1 : -1;
}

console.log(`Part 2: ${position}`);
