from selenium import webdriver
from selenium.webdriver.common.by import By

# Selenium docs: https://selenium-python.readthedocs.io/
# Chrome Driver: https://sites.google.com/chromium.org/driver/


driver = webdriver.get('https://google.com')

#driver.get_element_by_xpath("//button[@id='L2AGLb").click()
driver.find_element(by=By.XPATH, value="//button[@id='L2AGLb").click()

# haku = driver.get_element_by_name('q')
haku = driver.find_element(by=By.NAME, value='q')
haku.send_keys('ChromeDriver')
haku.submit()